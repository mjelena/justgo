Opis
Web aplikacija ima za cilj da pomogne turistima u obilaženju gradova. Korisnici mogu da pronadju osnovne informacije o gradu, o mjestima koje trebaju posjetiti, kao i da postavljaju svoje slike iz obilaska. Mjesta koja su im se najviše svidjela mogu da ocijene tako da bi drugi korisnici imali bolji uvid o zanimljivosti odredjenih mjesta u gradu.

Tehnologije
MEAN stack (MongoDB, Express, AngularJS, Node.js)

Web aplikacija ima frontend i backend validaciju, autentifikaciju i autorizaciju, kao i odgovarajuci error handling.